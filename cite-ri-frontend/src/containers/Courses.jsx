import React, { Fragment, useEffect, useState } from 'react';
import { Link } from "react-router-dom";
import axios from 'axios';
import { MDBCardHeader, MDBCardBody, MDBDataTableV5 } from 'mdbreact';
import { MDBContainer} from "mdbreact";

const Courses = () => {

  const [loading, setLoading] = useState(true);
  const [cursos, setCursos] = useState([]);
  const cargarCursos = () => {
    axios.get('http://localhost:8000/api/curso')
    .then(res => {
      
     
      for (var container in res.data) {
        var id = res.data[container].id;
        var obj = {addBtn :
          <div class="btn-group mb-2 mr-2">
            <button class="btn btn-outline-primary dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Acciones</button>
            <div class="dropdown-menu">
                  <Link className="edit-link" to={"/editcourse/" + id}>
                    <a class="dropdown-item">Editar</a>
                  </Link>
                  <Link className="edit-link" to={"/viewcourse/" + id}>
                    <a class="dropdown-item">Mostrar</a>
                  </Link>
            </div>
          </div>
           }
        //console.log(id);
        res.data[container] = Object.assign(res.data[container], obj);
      }
      setLoading(false)
      setCursos(res.data)

    })
    .catch((error) => {
      setLoading(false)
      console.log(error);
    })
  }
  useEffect(cargarCursos, [])

    const data = {
        columns: [
          {
            label: 'Nombre',
            field: 'nombre',
            sort: 'asc',
            width: 150
          },
          {
            label: 'Fecha inicio',
            field: 'apertura_curso',
            sort: 'asc',
            width: 200
          },
          {
            label: 'Fecha fin',
            field: 'cierre_curso',
            sort: 'asc',
            width: 200
          },
          {
            label: 'Código',
            field: 'codigo',
            sort: 'asc',
            width: 270
          },
          {
            label: 'Estado',
            field: 'estado',
            sort: 'asc',
            width: 150
          },
          {
            label: 'Acciones',
            field: 'addBtn',
            sort: 'asc',
            width: 100
          }
        ],
        rows: cursos
          //{
            //name: 'Tiger Nixon',
            //position: <Link to="/">Link</Link>,
            //position: 'System Architect',
            //office: 'Edinburgh',
            //age: '61',
            //date: '2011/04/25',
            //salary: '$320',
            //addBtn : <button className="btn btn-primary" onClick={() => this.addToCart()}>Add</button>
            
          //},
          
          
        
      };
    
      return (
        <MDBContainer>
          <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
          <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
              <MDBCardHeader>
                <div class="form-row">
                  <div class="form-group col-md-10">
                    <br></br>
                    <h2>CURSOS</h2>
                  </div>
                  <div class="form-group col-md-2">
                  <br></br>
                    <Link to={"/createcourses/"}>
                      <button type="button" class="btn btn-success">Crear Curso</button>
                    </Link>
                  </div>
                </div>
              </MDBCardHeader>
              <MDBCardBody>
               
                  <MDBDataTableV5 
                    hover 
                    data={data} 
                    filter='office' 
                    proSelect 
                    searchTop 
                    searchBottom={false} 
                  />;

              </MDBCardBody>
              
            </MDBContainer>
        

          
        
      );
    
}

export default Courses;
