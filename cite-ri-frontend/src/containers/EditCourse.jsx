import React, {Fragment, useEffect, useState} from 'react';
import { Link } from "react-router-dom";
import axios from 'axios'
import Swal from 'sweetalert2';



const EditCourse = (props) => {
    var id = props.match.params.id;

    const [datos, setDatos] = useState({
        codigo: '',
        nombre: '',
        apertura_curso: '',
        cierre_curso: '',
        fecha_inscripcion: '',
        apertura_psicol: '',
        cierre_psicol: '',
        apertura_medico: '',
        cierre_medico: '',
        apertura_fisico: '',
        cierre_fisico: '',
        inauguracion: '',
        apertura_tierra: '',
        cierre_tierra: '',
        apertura_saltos: '',
        cierre_saltos: ''
    })

    const CargarCurso = () => {
        axios.get('http://localhost:8000/api/curso/get2/'+id)
       .then(res => {
           
         setDatos(res.data)
   
             

       })
       .catch((error) => {
         console.log(error);
       })

       
       

     }

    
     useEffect(CargarCurso,[])    // carga de cursos


    const validaFecha = (fechIn, fechFin, event) => {
        var b = 0;
        if(fechIn > fechFin){
            event.style.visibility = 'visible';
            console.log("las fechas estan mal");
            b = 1;
        }
        if(fechIn < fechFin){
            console.log("las fechas estan bien");
            event.style.visibility = 'hidden';
        }
        return b;
    }
    
    const handleInputChange = (event) => {
        // console.log(event.target.name)
        // console.log(event.target.value)  // obtenemos valor
        var input = document.getElementById('fech_salto');
            console.log(datos.nombre);
            console.log(input);
            if(datos.nombre==="Paracaidismo" || datos.nombre==="Salto Libre"){
                input.style.visibility = 'visible';
            }else if (datos.nombre==="Plegador"){
                input.style.visibility = 'hidden';
            }
        setDatos({
            ...datos,
            [event.target.name] : event.target.value
        })
        
    }
    const styles = {
        color: "red",
        visibility: 'hidden'
    };

    const enviarDatos = (event) => {

        var apertura_curso = document.getElementById('apertura_curso').value;
        var cierre_curso = document.getElementById('cierre_curso').value;
        var spn_curso = document.getElementById('fechas_cursos');

        var fecha_inscripcion = document.getElementById('fecha_inscripcion').value;
        var spn_inscrip = document.getElementById('fechas_inscrip');

        var apertura_psicol = document.getElementById('apertura_psicol').value;
        var cierre_psicol = document.getElementById('cierre_psicol').value;
        var spn_psciol = document.getElementById('fechas_psico');

        var apertura_medico = document.getElementById('apertura_medico').value;
        var cierre_medico = document.getElementById('cierre_medico').value;
        var spn_medico = document.getElementById('fechas_medico');

        var apertura_fisico = document.getElementById('apertura_fisico').value;
        var cierre_fisico = document.getElementById('cierre_fisico').value;
        var spn_fisico = document.getElementById('fechas_fisico');

        var inauguracion = document.getElementById('inauguracion').value;
        var spn_inaugur = document.getElementById('fechas_inaugura');

        var apertura_tierra = document.getElementById('apertura_tierra').value;
        var cierre_tierra = document.getElementById('cierre_tierra').value;
        var spn_tierra = document.getElementById('fechas_tierra');

        var apertura_saltos = document.getElementById('apertura_saltos').value;
        var cierre_saltos = document.getElementById('cierre_saltos').value;
        var spn_saltos = document.getElementById('fechas_saltos');

        console.log(spn_medico);
        var band = 0;
        band = band + validaFecha(apertura_curso, cierre_curso, spn_curso);
        band = band + validaFecha(apertura_psicol, cierre_psicol, spn_psciol);
        band = band + validaFecha(apertura_medico, cierre_medico, spn_medico);
        band = band + validaFecha(apertura_fisico, cierre_fisico, spn_fisico);
        band = band + validaFecha(apertura_tierra, cierre_tierra, spn_tierra);
        if(band == 0){
            band = band + validaFecha(apertura_curso, fecha_inscripcion, spn_inscrip);
            band = band + validaFecha(fecha_inscripcion, apertura_psicol, spn_psciol);
            band = band + validaFecha(cierre_psicol, apertura_medico, spn_medico);
            band = band + validaFecha(cierre_medico, apertura_fisico, spn_fisico);
            band = band + validaFecha(cierre_fisico, inauguracion, spn_inaugur);
            band = band + validaFecha(inauguracion, apertura_tierra, spn_tierra);
            if(band == 0){
                if(document.getElementById('nombre').value==="Paracaidismo" || document.getElementById('nombre').value ==="Salto Libre"){
                    band = band + validaFecha(apertura_saltos, cierre_saltos, spn_saltos);
                    if(band == 0){
                        band = band + validaFecha(cierre_tierra, apertura_saltos, spn_saltos);
                        if(band == 0 ){
                            band = band + validaFecha(cierre_saltos, cierre_curso, spn_saltos);
                        }
                    } 
                    
                }
                if(document.getElementById('nombre').value==="Plegador"){
                    band = band + validaFecha(cierre_tierra, cierre_curso, spn_tierra);
                }
            }
        }
        

        event.preventDefault()
            // console.log('enviando datos...' + datos.nombre + ' ' + datos.apertura_curso)
            console.log(datos)
            axios.post('http://localhost:8000/api/curso/actualizar/'+ id, datos)
            .then(res => Swal.fire(res.data.nombre)
            );

        

        
        

    }



    return (
        <div>
            <h3>Editar Curso</h3>
            <form onSubmit={enviarDatos}>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="inputFecha4">Nombre de Curso</label>
                        <input type="text" class="form-control" id="nombre" name="nombre" value={datos.nombre} readonly="readonly"></input>
                        <label for="inputFecha4">Código de Curso</label>
                        <input type="text" class="form-control" id="codigo" name="codigo" value={datos.codigo} readonly="readonly"></input>
                    </div>

                    <div class="form-group col-md-6">
                        <label for="inputFecha4">Fechas de inicio y fin de Curso</label>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="inputFecha4">Fecha Inicio</label>
                                <input type="Date" class="form-control" id="apertura_curso" name="apertura_curso" value={datos.apertura_curso} onChange={handleInputChange} placeholder="Fecha" required></input>
                                <span style={styles} id="fechas_cursos" class="error-message">*Revise las fechas.</span>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="inputFecha4">Fecha Fin</label>
                                <input type="Date" class="form-control" id="cierre_curso" name="cierre_curso" value={datos.cierre_curso} onChange={handleInputChange} placeholder="Fecha"  required></input>
                            </div>
                            
                        </div>
                        
                    
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="inputFecha4">Fecha de Inscripción</label>
                        <input type="Date" class="form-control" id="fecha_inscripcion" name="fecha_inscripcion" value={datos.fecha_inscripcion} onChange={handleInputChange} placeholder="Fecha"  required></input>
                        <span style={styles} id="fechas_inscrip" class="error-message" >*Revise las fechas.</span>
                    </div>

                    <div class="form-group col-md-6">
                        <label for="inputFecha4">Fechas Examen Psicológico</label>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="inputFecha4">Fecha Inicio</label>
                                <input type="Date" class="form-control" id="apertura_psicol" name="apertura_psicol" value={datos.apertura_psicol} onChange={handleInputChange} placeholder="Fecha"  required></input>
                                <span style={styles} id="fechas_psico" class="error-message">*Revise las fechas.</span>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="inputFecha4">Fecha Fin</label>
                                <input type="Date" class="form-control" id="cierre_psicol" name="cierre_psicol" value={datos.cierre_psicol} onChange={handleInputChange} placeholder="Fecha"  required></input>
                            </div>
                        </div>
                        
                    
                    </div>
                </div>

                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="inputFecha4">Fechas de Examen Médico</label>
                        
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="inputFecha4">Fecha Inicio</label>
                                <input type="Date" class="form-control" id="apertura_medico" name="apertura_medico" value={datos.apertura_medico} onChange={handleInputChange} placeholder="Fecha"  required></input>
                                <span style={styles} id="fechas_medico" class="error-message">*Revise las fechas.</span>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="inputFecha4">Fecha Fin</label>
                                <input type="Date" class="form-control" id="cierre_medico" name="cierre_medico" value={datos.cierre_medico} onChange={handleInputChange} placeholder="Fecha"  required></input>
                            </div>
                        </div>
                    </div>

                    <div class="form-group col-md-6">
                        <label for="inputFecha4">Fechas Examen Físico</label>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="inputFecha4">Fecha Inicio</label>
                                <input type="Date" class="form-control" id="apertura_fisico" name="apertura_fisico" value={datos.apertura_fisico} onChange={handleInputChange} placeholder="Fecha"  required></input>
                                <span style={styles} id="fechas_fisico" class="error-message">*Revise las fechas.</span>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="inputFecha4">Fecha Fin</label>
                                <input type="Date" class="form-control" id="cierre_fisico" name="cierre_fisico" value={datos.cierre_fisico} onChange={handleInputChange} placeholder="Fecha"  required></input>
                            </div>
                        </div>
                        
                    
                    </div>
                </div>

                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="inputFecha4">Inauguración de Curso</label>
                        
                        <input type="Date" class="form-control" id="inauguracion" name="inauguracion" value={datos.inauguracion} onChange={handleInputChange} placeholder="Fecha"  required></input>
                        <span style={styles} id="fechas_inaugura" class="error-message">*Revise las fechas.</span>
                    </div>

                    <div class="form-group col-md-6">
                        <label for="inputFecha4">Fechas de Entrenamiento en Tierra</label>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="inputFecha4">Fecha Inicio</label>
                                <input type="Date" class="form-control" id="apertura_tierra" name="apertura_tierra" value={datos.apertura_tierra} onChange={handleInputChange} placeholder="Fecha"  required></input>
                                <span style={styles} id="fechas_tierra" class="error-message">*Revise las fechas.</span>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="inputFecha4">Fecha Fin</label>
                                <input type="Date" class="form-control" id="cierre_tierra" name="cierre_tierra" value={datos.cierre_tierra} onChange={handleInputChange} placeholder="Fecha"  required></input>
                            </div>
                        </div>
                        
                    </div>
                </div>


                <div class="form-row" id="fech_salto">
                    
                    <div class="form-group col-md-6" >
                        <label for="inputFecha4">Fechas de Entrenamiento en Saltos</label>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="inputFecha4">Fecha Inicio</label>
                                <input type="Date" class="form-control" id="apertura_saltos" name="apertura_saltos" value={datos.apertura_saltos} onChange={handleInputChange} placeholder="Fecha"></input>
                                <span style={styles} id="fechas_saltos" class="error-message">*Revise las fechas.</span>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="inputFecha4">Fecha Fin</label>
                                <input type="Date" class="form-control" id="cierre_saltos" name="cierre_saltos" value={datos.cierre_saltos} onChange={handleInputChange} placeholder="Fecha"></input>
                            </div>
                        </div>
                        
                    </div>
                </div>

            
                <div class="form-row">
                    <div class="form-group col-md-2">
                        <button type="submit" class="btn btn-primary">Aceptar Cambios</button>
                    </div>
                    <div class="form-group col-md-10">
                    <Link to="/courses">
                        <button class="btn btn-danger">Regresar</button>
                    </Link>
                    </div>
                </div>
            </form>

            
        </div>
        

    );
}

export default EditCourse;
