import React, { Fragment, useEffect, useState }  from "react";
import axios from 'axios';
import { Link } from "react-router-dom";
//import customer from "Customer"


const ViewCourse = (props) => {

  var id = props.match.params.id;
  const [loading, setLoading] = useState(true);
  const [cursos, setCursos] = useState();

 
  const CargarCursos = () => {
     axios.get('http://localhost:8000/api/curso/get/'+id)
    .then(res => {
        
        setLoading(false)
      setCursos(res.data)

    })
    .catch((error) => {
        setLoading(false)
      console.log(error);
    })

    var input = document.getElementById('fech_salto');
            console.log(cursos && cursos.nombre);
            console.log(input);
            if(cursos && cursos.nombre==="Paracaidismo" || cursos && cursos.nombre==="Salto Libre"){
                input.style.visibility = 'visible';
            }else if (cursos && cursos.nombre==="Plegador"){
                input.style.visibility = 'hidden';
            }
  }
  //{this.props.users.name} is {this.props.users.age} years old.
  useEffect(CargarCursos,[])
  const data = {
    columns: cursos
  }
    return (
        <div>
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
             
           
            <div>
                <h3 align="center">DATOS DE CURSO</h3>
            </div>
            <br/><br/>
            <form>
            <div class="form-group row">
                <label for="nombre" class="col-sm-4 col-form-label">Nombre del Curso</label>
                <div class="col-sm-8">
                <input type="text" readonly  class="form-control-plaintext" id="nombre" name="nombre" value={cursos && cursos.nombre + "   (" + cursos.codigo + ")"  } ></input>
                </div>
            </div>
            
            <div class="form-group row">
                <div class="form-group col-md-6">
                    <div class="form-group row">
                            <label for="apertura_curso" class="col-sm-6 col-form-label">Fecha de Apertura de Curso:</label>
                        <div class="col-sm-6">
                            <input type="text" readonly class="form-control-plaintext" id="apertura_curso" value={cursos && cursos.apertura_curso}></input>
                        </div>
                    </div>
                </div>
                    <div class="form-group col-md-6">
                        <div class="form-group row">
                            <label for="apertura_curso" class="col-sm-6 col-form-label">Fecha de Cierre de Curso:</label>
                        <div class="col-sm-6">
                            <input type="text" readonly class="form-control-plaintext" id="apertura_curso" value={cursos && cursos.cierre_curso}></input>
                        </div>
                    </div>
                </div>
            </div>



            <div class="form-group row">
                <label for="fecha_inscripcion" class="col-sm-4 col-form-label">Fecha de Inscripciones</label>
                <div class="col-sm-8">
                <input type="text" readonly class="form-control-plaintext" id="fecha_inscripcion" value={cursos && cursos.fecha_inscripcion}></input>
                </div>
            </div>
         

            <div class="form-group row">
                <div class="form-group col-md-12">
                    <label for="inputFecha4">Examen Psicológico</label>
                </div>
                <div class="form-group col-md-6">
                    <div class="form-group row">
                            <label for="apertura_curso" class="col-sm-4 col-form-label">Fecha Inicio:</label>
                        <div class="col-sm-8">
                            <input type="text" readonly class="form-control-plaintext" id="apertura_curso" value={cursos && cursos.apertura_psicol}></input>
                        </div>
                    </div>
                </div>
                    <div class="form-group col-md-6">
                        <div class="form-group row">
                            <label for="apertura_curso" class="col-sm-4 col-form-label">Fecha Fin:</label>
                        <div class="col-sm-8">
                            <input type="text" readonly class="form-control-plaintext" id="apertura_curso" value={cursos && cursos.cierre_psicol}></input>
                        </div>
                    </div>
                </div>
            </div>






            <div class="form-group row">
                <div class="form-group col-md-12">
                    <label for="inputFecha4">Examen Medico</label>
                </div>
                <div class="form-group col-md-6">
                    <div class="form-group row">
                            <label for="apertura_curso" class="col-sm-4 col-form-label">Fecha Inicio:</label>
                        <div class="col-sm-8">
                            <input type="text" readonly class="form-control-plaintext" id="apertura_curso" value={cursos && cursos.apertura_medico}></input>
                        </div>
                    </div>
                </div>
                    <div class="form-group col-md-6">
                        <div class="form-group row">
                            <label for="apertura_curso" class="col-sm-4 col-form-label">Fecha Fin:</label>
                        <div class="col-sm-8">
                            <input type="text" readonly class="form-control-plaintext" id="apertura_curso" value={cursos && cursos.cierre_medico}></input>
                        </div>
                    </div>
                </div>
            </div>




            <div class="form-group row">
                <div class="form-group col-md-12">
                    <label for="inputFecha4">Examen Físico</label>
                </div>
                <div class="form-group col-md-6">
                    <div class="form-group row">
                            <label for="apertura_curso" class="col-sm-4 col-form-label">Fecha Inicio:</label>
                        <div class="col-sm-8">
                            <input type="text" readonly class="form-control-plaintext" id="apertura_curso" value={cursos && cursos.apertura_fisico}></input>
                        </div>
                    </div>
                </div>
                    <div class="form-group col-md-6">
                        <div class="form-group row">
                            <label for="apertura_curso" class="col-sm-4 col-form-label">Fecha Fin:</label>
                        <div class="col-sm-8">
                            <input type="text" readonly class="form-control-plaintext" id="apertura_curso" value={cursos && cursos.cierre_fisico}></input>
                        </div>
                    </div>
                </div>
            </div>



        
            <div class="form-group row">
                <label for="inauguracion" class="col-sm-4 col-form-label">Fecha de Inauguración</label>
                <div class="col-sm-8">
                <input type="text" readonly class="form-control-plaintext" id="inauguracion" value={cursos && cursos.inauguracion}></input>
                </div>
            </div>


            <div class="form-group row">
                <div class="form-group col-md-12">
                    <label for="inputFecha4">Entrenamiento en Tierra</label>
                </div>
                <div class="form-group col-md-6">
                    <div class="form-group row">
                            <label for="apertura_curso" class="col-sm-4 col-form-label">Fecha Inicio:</label>
                        <div class="col-sm-8">
                            <input type="text" readonly class="form-control-plaintext" id="apertura_curso" value={cursos && cursos.apertura_tierra}></input>
                        </div>
                    </div>
                </div>
                    <div class="form-group col-md-6">
                        <div class="form-group row">
                            <label for="apertura_curso" class="col-sm-4 col-form-label">Fecha Fin:</label>
                        <div class="col-sm-8">
                            <input type="text" readonly class="form-control-plaintext" id="apertura_curso" value={cursos && cursos.cierre_tierra}></input>
                        </div>
                    </div>
                </div>
            </div>




            <div id="fech_salto" class="form-group row" hidden>
                <div class="form-group col-md-12">
                    <label for="inputFecha4">Entrenamiento de Saltos</label>
                </div>
                <div class="form-group col-md-6">
                    <div class="form-group row">
                            <label for="apertura_saltos" class="col-sm-4 col-form-label">Fecha Inicio:</label>
                        <div class="col-sm-8">
                            <input type="text" readonly class="form-control-plaintext" id="apertura_saltos" value={cursos && cursos.apertura_saltos}></input>
                        </div>
                    </div>
                </div>
                    <div class="form-group col-md-6">
                        <div class="form-group row">
                            <label for="apertura_curso" class="col-sm-4 col-form-label">Fecha Fin:</label>
                        <div class="col-sm-8">
                            <input type="text" readonly class="form-control-plaintext" id="apertura_curso" value={cursos && cursos.cierre_saltos}></input>
                        </div>
                    </div>
                </div>
            </div>

                <div class="form-row">
                    <div class="form-group col-md-10">
                    <Link to="/courses">
                        <button class="btn btn-danger">Regresar</button>
                    </Link>
                    </div>
                </div>
            </form>
        </div>
    );
};

export default ViewCourse;
