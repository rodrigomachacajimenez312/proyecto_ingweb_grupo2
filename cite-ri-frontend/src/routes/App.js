import React, { useEffect } from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import Layout from "../components/Layout";
import SignIn from "../containers/SignIn"
import Students from "../containers/Students";
import Courses from "../containers/Courses";
import CreateCourses from "../containers/CreateCourses";
import ViewCourse from "../containers/ViewCourse";
import EditCourse from "../containers/EditCourse";
import Materia from "../containers/Materia";
import CertificadoGrupal from "../containers/CertificadoGrupal";
import CertificadoIndividual from "../containers/CertificadoIndividual";


const App = () => {
    return (
        <BrowserRouter>
            <Layout>
                <Switch>
                    <Route exact path="/sigin" component={SignIn} />
                    <Route exact path="/" component={Materia} />
                    <Route exact path="/students" component={Students} />
                    <Route exact path="/courses" component={Courses} />
                    <Route exact path="/createcourses" component={CreateCourses} />
                    <Route exact path="/viewcourse/:id" component={ViewCourse} />
                    <Route exact path="/editcourse/:id" component={EditCourse} />
                    <Route exact path="/certificado/individual" component={CertificadoIndividual} />
                    <Route exact path="/certificado/grupal" component={CertificadoGrupal} />
                </Switch>
            </Layout>
        </BrowserRouter>
    );
};

export default App;
