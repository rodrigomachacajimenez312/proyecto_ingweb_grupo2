<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class alumno extends Model
{
    public $table="student";

    protected $fillable = [
        'n_casco',
        'nombre',
        'grado',
        'evaluador',
        'fecha'
    ];
}
