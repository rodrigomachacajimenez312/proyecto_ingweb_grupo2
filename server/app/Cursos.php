<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cursos extends Model
{
    //
    protected $fillable = ['codigo', 'estado_id', 'nombre', 'apertura_curso', 'cierre_curso', 'fecha_inscripcion', 'apertura_psicol', 'cierre_psicol', 'apertura_medico', 'cierre_medico', 'apertura_fisico', 'cierre_fisico', 'inauguracion', 'apertura_tierra', 'cierre_tierra', 'apertura_saltos', 'cierre_saltos'];
}
