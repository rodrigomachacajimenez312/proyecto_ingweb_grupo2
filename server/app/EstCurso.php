<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EstCurso extends Model
{
    //
    protected $fillable = ['estado, curso_id'];
}
