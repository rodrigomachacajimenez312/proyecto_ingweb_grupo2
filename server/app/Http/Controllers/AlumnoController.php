<?php

namespace App\Http\Controllers;

use App\Alumno as Model;
use Illuminate\Http\Request;

class AlumnoController extends Controller
{
    public function index()
    {
        return $this->jsonCollection(Model::all());
    }

    public function store(Request $request)
    {
        $modelo = Model::create($request->all());
        return $this->jsonResource($modelo);
    }

    public function show($model)
    {
        $model = Model::findOrFail($model);
        return $this->jsonResource($model);
    }

    public function update(Request $request, $id)
    {
        $model = Model::findOrFail($id);
        $model->update($request->all());
        $salida = Model::findOrFail($id);
        return $this->jsonResource($salida);
    }

    public function destroy($id)
    {
        $model = Model::findOrFail($id);
        $model->delete();
        return response()->json([], 200);
    }

    //Estructura prara un registro 
    private function jsonResource($data)
    {
        return response()->json(
            collect([
                'id' => $data->id,
                'postulantes_id' =>  $data->postulantes_id,
                'cursos_id' =>  $data->cursos_id,
                'Dato_extra' => 'Enviado',
            ])
        );
    }

    //Estructura para varios registro 
    private function jsonCollection($datas)
    {
        $aux = collect();
        foreach ($datas as $data){
            $aux->push([
                'id' => $data->id,
                'postulantes_id' =>  $data->postulantes_id,
                'cursos_id' =>  $data->cursos_id,
            ]);
        }
        return response()->json($aux);
    }
}
