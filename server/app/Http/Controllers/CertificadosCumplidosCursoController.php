<?php

namespace App\Http\Controllers;

use App\CertificadosCumplidosCurso as Model;
use Illuminate\Http\Request;

class CertificadosCumplidosCursoController extends Controller
{
    public function index()
    {
        return $this->jsonCollection(Model::all());
    }

    public function store(Request $request)
    {
        $modelo = Model::create($request->all());
        return $this->jsonResource($modelo);
    }

    public function show($model)
    {
        $model = Model::findOrFail($model);
        return $this->jsonResource($model);
    }

    public function update(Request $request, $id)
    {
        $model = Model::findOrFail($id);
        $model->update($request->all());
        $salida = Model::findOrFail($id);
        return $this->jsonResource($salida);
    }

    public function destroy($id)
    {
        $model = Model::findOrFail($id);
        $model->delete();
        return response()->json([], 200);
    }

    //Estructura prara un registro 
    private function jsonResource($data)
    {
        return response()->json(
            collect([
                'id' => $data->id,
                'numeros' =>  $data->numeros,
                'descripcion' =>  $data->descripcion,
                // 'alumnos_id' =>  $data->alumnos_id,
            ])
        );
    }

    //Estructura para varios registro 
    private function jsonCollection($datas)
    {
        $aux = collect();
        foreach ($datas as $data){
            $aux->push([
                'id' => $data->id,
                'numeros' =>  $data->numeros,
                'descripcion' =>  $data->descripcion,
                'alumnos_id' =>  $data->alumnos_id,
            ]);
        }
        return response()->json($aux);
    }
}
