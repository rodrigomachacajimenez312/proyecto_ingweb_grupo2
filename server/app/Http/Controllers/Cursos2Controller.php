<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use App\Cursos2 as Model;
use Illuminate\Http\Request;

class Cursos2Controller extends Controller
{
    public function index()
    {

        $resultado = DB::table('cursos2s')
        ->join('EstCurso','cursos2s.estado_id','=','EstCurso.id')
        ->select('cursos2s.id', 'nombre', 'codigo', 'apertura_curso', 'cierre_curso', 'estado') 
        ->get();
   
                        
        return $this -> jsonCollection($resultado);
    }

    public function store(Request $request)
    {
        $nombre = $request->input("nombre");
        $apertura_curso = $request->input("apertura_curso");
        $cierre_curso = $request->input("cierre_curso");
        $fecha_inscripcion = $request->input("fecha_inscripcion");
        $apertura_psicol = $request->input("apertura_psicol");
        $cierre_psicol = $request->input("cierre_psicol");
        $apertura_medico = $request->input("apertura_medico");
        $cierre_medico = $request->input("cierre_medico");
        $apertura_fisico = $request->input("apertura_fisico");
        $cierre_fisico = $request->input("cierre_fisico");
        $inauguracion = $request->input("inauguracion");
        $apertura_tierra = $request->input("apertura_tierra");
        $cierre_tierra = $request->input("cierre_tierra");
        $apertura_saltos = $request->input("apertura_saltos");
        $cierre_saltos = $request->input("cierre_saltos");
        
        $error=false;

        list($anio1, $mes1, $dia1) = explode("-",$apertura_curso);

        // Apertura y cierre de cursos
        if(! $this->verifica_tiempos($apertura_curso,$cierre_curso)){
            $error = true;
        }
        if(! $this->verifica_tiempos($apertura_psicol,$cierre_psicol)){
            $error = true;
        }
        if(! $this->verifica_tiempos($apertura_medico,$cierre_medico)){
            $error = true;
        }
        if(! $this->verifica_tiempos($apertura_fisico,$cierre_fisico)){
            $error = true;
        }
        if(! $this->verifica_tiempos($apertura_tierra,$cierre_tierra)){
            $error = true;
        }

        

        // actividad antes de otra
        if(! $this->verifica_tiempos($apertura_curso,$fecha_inscripcion)){
            $error = true;
        }
        if(! $this->verifica_tiempos($fecha_inscripcion,$apertura_psicol)){
            $error = true;
        }
        if(! $this->verifica_tiempos($cierre_psicol,$apertura_medico)){
            $error = true;
        }
        if(! $this->verifica_tiempos($cierre_medico,$apertura_fisico)){
            $error = true;
        }
        if(! $this->verifica_tiempos($cierre_fisico,$inauguracion)){
            $error = true;
        }
        if(! $this->verifica_tiempos($inauguracion,$apertura_tierra)){
            $error = true;
        }
        if ($nombre==="Paracaidismo" || $nombre==="Salto Libre"){
            if(! $this->verifica_tiempos($apertura_saltos,$cierre_saltos)){
                $error = true;
            }
            if(! $this->verifica_tiempos($cierre_tierra,$apertura_saltos)){
                $error = true;
            }
            if(! $this->verifica_tiempos($cierre_saltos,$cierre_curso)){
                $error = true;
            }

        }elseif ($nombre==="Plegador"){
            $request['apertura_saltos']=null;
            $request['cierre_saltos']=null;
            if(! $this->verifica_tiempos($cierre_tierra,$cierre_curso)){
                $error = true;
            }
        }else{
            $error = true;
        }

        if($error == false){
            $cont=1;

            $consulta = DB::table('cursos2s')
            ->select('id','apertura_curso') 
            ->where('nombre','=',$nombre)
            ->get();
            //$consulta = Model::where('nombre', '=', $nombre);
            foreach($consulta as $c){
                $fecha = $c->apertura_curso;
                
                list($anio, $mes, $dia) = explode("-",$fecha);
                if($anio==$anio1){
                    $cont++;
                }
            }
            
            $modelo = Model::create($request->all() + ['codigo' => $this->genCode($nombre,$cont)] + ['estado_id' => 1]);
            //$idCurs = Model::latest('id')->first();
            //$estado = MC::create(['estado' => '']);
            return $this->acept();
            //return $consulta;
        }else{
            return $this->error();
            //return $request['apertura_saltos'];
        }
        
    }




    public function show($model)
    {
        $resultado = DB::table('cursos2s')
        ->join('EstCurso','cursos2s.estado_id','=','EstCurso.id')
        ->select('cursos2s.id', 'nombre', 'codigo', 'apertura_curso', 'cierre_curso', 'estado',
        'fecha_inscripcion', 'apertura_psicol', 'cierre_psicol', 'apertura_medico', 'cierre_medico', 'apertura_fisico', 'cierre_fisico',
        'inauguracion', 'apertura_tierra', 'cierre_tierra', 'apertura_saltos', 'cierre_saltos') 
        ->where('cursos2s.id','=',$model)
        ->first();

        return $this->jsonResource($resultado);
    }

    public function show2($model)
    {
        $resultado = DB::table('cursos2s')
        ->select('id', 'nombre', 'codigo', 'apertura_curso', 'cierre_curso',
        'fecha_inscripcion', 'apertura_psicol', 'cierre_psicol', 'apertura_medico', 'cierre_medico', 'apertura_fisico', 'cierre_fisico',
        'inauguracion', 'apertura_tierra', 'cierre_tierra', 'apertura_saltos', 'cierre_saltos') 
        ->where('cursos2s.id','=',$model)
        ->first();

        return $this->jsonResource2($resultado);
    }

    public function update(Request $request, $id)
    {

        $nombre = $request->input("nombre");
        $apertura_curso = $request->input("apertura_curso");
        $cierre_curso = $request->input("cierre_curso");
        $fecha_inscripcion = $request->input("fecha_inscripcion");
        $apertura_psicol = $request->input("apertura_psicol");
        $cierre_psicol = $request->input("cierre_psicol");
        $apertura_medico = $request->input("apertura_medico");
        $cierre_medico = $request->input("cierre_medico");
        $apertura_fisico = $request->input("apertura_fisico");
        $cierre_fisico = $request->input("cierre_fisico");
        $inauguracion = $request->input("inauguracion");
        $apertura_tierra = $request->input("apertura_tierra");
        $cierre_tierra = $request->input("cierre_tierra");
        $apertura_saltos = $request->input("apertura_saltos");
        $cierre_saltos = $request->input("cierre_saltos");
        
        $error=false;

        list($anio1, $mes1, $dia1) = explode("-",$apertura_curso);

        // Apertura y cierre de cursos
        if(! $this->verifica_tiempos($apertura_curso,$cierre_curso)){
            $error = true;
        }
        if(! $this->verifica_tiempos($apertura_psicol,$cierre_psicol)){
            $error = true;
        }
        if(! $this->verifica_tiempos($apertura_medico,$cierre_medico)){
            $error = true;
        }
        if(! $this->verifica_tiempos($apertura_fisico,$cierre_fisico)){
            $error = true;
        }
        if(! $this->verifica_tiempos($apertura_tierra,$cierre_tierra)){
            $error = true;
        }

        

        // actividad antes de otra
        if(! $this->verifica_tiempos($apertura_curso,$fecha_inscripcion)){
            $error = true;
        }
        if(! $this->verifica_tiempos($fecha_inscripcion,$apertura_psicol)){
            $error = true;
        }
        if(! $this->verifica_tiempos($cierre_psicol,$apertura_medico)){
            $error = true;
        }
        if(! $this->verifica_tiempos($cierre_medico,$apertura_fisico)){
            $error = true;
        }
        if(! $this->verifica_tiempos($cierre_fisico,$inauguracion)){
            $error = true;
        }
        if(! $this->verifica_tiempos($inauguracion,$apertura_tierra)){
            $error = true;
        }
        if ($nombre==="Paracaidismo" || $nombre==="Salto Libre"){
            if(! $this->verifica_tiempos($apertura_saltos,$cierre_saltos)){
                $error = true;
            }
            if(! $this->verifica_tiempos($cierre_tierra,$apertura_saltos)){
                $error = true;
            }
            if(! $this->verifica_tiempos($cierre_saltos,$cierre_curso)){
                $error = true;
            }

        }elseif ($nombre==="Plegador"){
            $request['apertura_saltos']=null;
            $request['cierre_saltos']=null;
            if(! $this->verifica_tiempos($cierre_tierra,$cierre_curso)){
                $error = true;
            }
        }else{
            $error = true;
        }

        if($error == false){
            
            
            $model = Model::findOrFail($id);
            $model->update($request->all());
            $salida = Model::findOrFail($id);

            return $this->acept2();
            //return $consulta;
        }else{
            return $this->error();
            //return $request['apertura_saltos'];
        }

        //return $this->jsonResource($salida);
    }

    public function destroy($id)
    {
        $model = Model::findOrFail($id);
        $model->delete();
        return response()->json([], 200);
    }











    

    //Estructura prara un registro 
    private function jsonResource($data)
    {
        if( $data->cierre_saltos != null ||  $data->apertura_saltos != null){
            $date1 = date_create($data->apertura_saltos);
            $date1 = date_format($date1, "d-M-y");

            $date2 = date_create($data->cierre_saltos);
            $date2 = date_format($date2, "d-M-y");
        }else{
            $date1 = null;
            $date2 = null;
        }
        

        return response()->json(
            collect([
                'id' => $data->id,
                'estado' => $data->estado,
                'codigo' =>  $data->codigo,
                'nombre' =>  $data->nombre,
                'apertura_curso' =>  date_format(date_create($data->apertura_curso),"d-M-y"),
                'cierre_curso' =>  date_format(date_create($data->cierre_curso),"d-M-y"),
                'fecha_inscripcion' =>  date_format(date_create($data->fecha_inscripcion),"d-M-y"),
                'apertura_psicol' =>  date_format(date_create($data->apertura_psicol),"d-M-y"),
                'cierre_psicol' =>  date_format(date_create($data->cierre_psicol),"d-M-y"),
                'apertura_medico' =>  date_format(date_create($data->apertura_medico),"d-M-y"),
                'cierre_medico' =>  date_format(date_create($data->cierre_medico),"d-M-y"),
                'apertura_fisico' =>  date_format(date_create($data->apertura_fisico),"d-M-y"),
                'cierre_fisico' =>  date_format(date_create($data->cierre_fisico),"d-M-y"),
                'inauguracion' =>  date_format(date_create($data->inauguracion),"d-M-y"),
                'apertura_tierra' =>  date_format(date_create($data->apertura_tierra),"d-M-y"),
                'cierre_tierra' =>  date_format(date_create($data->cierre_tierra),"d-M-y"),
                'apertura_saltos' =>  $date1,
                'cierre_saltos' =>  $date2
            ])
        );
    }

    private function jsonResource2($data)
    {

        return response()->json(
            collect([
                'id' => $data->id,
                'codigo' =>  $data->codigo,
                'nombre' =>  $data->nombre,
                'apertura_curso' =>  $data->apertura_curso,
                'cierre_curso' =>  $data->cierre_curso,
                'fecha_inscripcion' =>  $data->fecha_inscripcion,
                'apertura_psicol' =>  $data->apertura_psicol,
                'cierre_psicol' =>  $data->cierre_psicol,
                'apertura_medico' =>  $data->apertura_medico,
                'cierre_medico' =>  $data->cierre_medico,
                'apertura_fisico' =>  $data->apertura_fisico,
                'cierre_fisico' =>  $data->cierre_fisico,
                'inauguracion' =>  $data->inauguracion,
                'apertura_tierra' =>  $data->apertura_tierra,
                'cierre_tierra' =>  $data->cierre_tierra,
                'apertura_saltos' =>  $data->apertura_saltos,
                'cierre_saltos' =>  $data->cierre_saltos
            ])
        );
    }

    
    private function error()
    {
        return response()->json(
            collect([
                'nombre' => 'Verifique la consistencia de fechas.'
            ])
        );
    }
    private function acept()
    {
        return response()->json(
            collect([
                'nombre' => 'Registrado exitosamente'
            ])
        );
    }
    private function acept2()
    {
        return response()->json(
            collect([
                'nombre' => 'Actualizado exitosamente'
            ])
        );
    }
    //Estructura para varios registro 
    private function jsonCollection($datas)
    {
        $aux = collect();
        foreach ($datas as $data){
            $date1 = date_create($data->apertura_curso);
            $date1 = date_format($date1, "d-M-y");

            $date2 = date_create($data->cierre_curso);
            $date2 = date_format($date2, "d-M-y");

            $aux->push([
                'id' => $data->id,
                'codigo' =>  $data->codigo,
                'nombre' =>  $data->nombre,
                'apertura_curso' =>  $date1,
                'cierre_curso' =>  $date2,
                'estado' => $data->estado
            ]);
        }

        //->select('cursos2s.id', 'nombre', 'codigo', 'apertura_curso', 'cierre_curso', 'estado') 
        return response()->json($aux);
    }

    private function verifica_tiempos($date_inicio, $date_fin) {
        $date_inicio = strtotime($date_inicio);
        $date_fin = strtotime($date_fin);
        if (($date_fin > $date_inicio) )
            return true;
        return false;
     }
     // El codigo de cada curso sera automatico:  **curso+num de curso en el año + año actual** 
     // ej: Para012020 - Salto012020 - Plegador012020

     private function genCode($nombre,$nro){
        //date($format, $timestamp);
        if($nombre==="Paracaidismo"){
            $nombre = substr($nombre,0,4);
        }elseif($nombre==="Salto Libre"){
            $nombre = substr($nombre,0,5);
        }
        $Year = date("Y");
        $codigo=$nombre."0".$nro.$Year;
        return $codigo;
     }
}
