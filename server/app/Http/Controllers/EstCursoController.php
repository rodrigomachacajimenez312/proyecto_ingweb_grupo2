<?php

namespace App\Http\Controllers;

use App\EstCurso;
use Illuminate\Http\Request;

class EstCursoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\EstCurso  $estCurso
     * @return \Illuminate\Http\Response
     */
    public function show(EstCurso $estCurso)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\EstCurso  $estCurso
     * @return \Illuminate\Http\Response
     */
    public function edit(EstCurso $estCurso)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\EstCurso  $estCurso
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EstCurso $estCurso)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EstCurso  $estCurso
     * @return \Illuminate\Http\Response
     */
    public function destroy(EstCurso $estCurso)
    {
        //
    }
}
