<?php

namespace App\Http\Controllers;

use App\Materia as Model;
use Illuminate\Http\Request;

class MateriaController extends Controller
{
    public function index()
    {
        return $this->jsonCollection(Model::all());
    }

    public function store(Request $request)
    {
        $modelo = Model::create($request->all());
        return $this->jsonResource($modelo);
    }

    public function show(Model $model)
    {
        return $this->jsonResource($model);
    }

    public function update(Request $request, Model $model)
    {
        $model = Model::updated($request->all);
        return $this->jsonResource($model);
    }

    public function destroy(Model $model)
    {
        $model->delete();
        return response()->json(200);
    }

    //Estructura prara un registro 
    private function jsonResource($data)
    {
        return response()->json(
            collect([
                'id' => $data->id,
                'nombre' =>  $data->nombre,
                'descripcion' =>  $data->descripcion,
            ])
        );
    }

    //Estructura para varios registro 
    private function jsonCollection($datas)
    {
        $aux = collect();
        foreach ($datas as $data){
            $aux->push([
                'id' => $data->id,
                'nombre' =>  $data->nombre,
                'descripcion' =>  $data->descripcion,
            ]);
        }
        return response()->json($aux);
    }
}
