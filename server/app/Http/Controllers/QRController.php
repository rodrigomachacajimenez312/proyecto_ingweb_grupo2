<?php

namespace App\Http\Controllers;

use App\alumno as Model;
use Illuminate\Http\Request;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class QRController extends Controller
{
    public function generar_qr($id){
        $result = Model::where("id",$id)->get();

        if($result) {
            $nombre     = $result[0]->nombre;
            $n_casco    = $result[0]->n_casco;
            $evaluador  = $result[0]->evaluador;
            $grado      = $result[0]->grado;
            $url        = 'http://campus.cochabamba.emi.edu.bo/casco='.$n_casco.'/nombre='.$nombre.'/evaluador='.$evaluador.'/grado='.$grado;
            return QrCode::generate($url);
        } else {
            return [
                "statusCode"    => 500,
                "message"       => "Internal Server Error"
            ];
        }
    }

    public function mail_qr($id){
        $result = Model::where("id",$id)->get();

        if($result) {
            $to = 'alvarogalarzaalmanza@gmail.com';
            $subject = 'Prueba';
            $body = 'contenido';
            return QrCode::email($to, $subject, $body);
        } else {
            return [
                "statusCode"    => 500,
                "message"       => "Internal Server Error"
            ];
        }
    }
}
