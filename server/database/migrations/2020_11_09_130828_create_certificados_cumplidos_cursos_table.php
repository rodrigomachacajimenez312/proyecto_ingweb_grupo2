<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCertificadosCumplidosCursosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('certificados_cumplidos_cursos', function (Blueprint $table) {
            $table->id();
            $table->integer('numero');
            $table->text('descripcion');
            $table->unsignedBigInteger('alumnos_id');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('certificados_cumplidos_cursos');
    }
}
