<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCursos2sTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cursos2s', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedBigInteger('estado_id');
            $table->string('codigo', 100);
            $table->string('nombre', 191);
            $table->date('apertura_curso');
            $table->date('cierre_curso');
            $table->date('fecha_inscripcion');
            $table->date('apertura_psicol');
            $table->date('cierre_psicol');
            $table->date('apertura_medico');
            $table->date('cierre_medico');
            $table->date('apertura_fisico');
            $table->date('cierre_fisico');
            $table->date('inauguracion');
            $table->date('apertura_tierra');
            $table->date('cierre_tierra');
            $table->date('apertura_saltos')->nullable();
            $table->date('cierre_saltos')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cursos2s');
    }
}
