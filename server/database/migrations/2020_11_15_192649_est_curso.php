<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

use Illuminate\Support\Facades\DB;

class EstCurso extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('EstCurso', function (Blueprint $table) {
            $table->increments('id');
            $table->string('estado', 100);
            $table->timestamps();

            

        });

        DB::table('EstCurso')->insert(
            array(
             'estado' => 'En Progreso', 
            ) 
        ); 
        DB::table('EstCurso')->insert(
            array(
             'estado' => 'Habilitado', 
            ) 
        ); 
        DB::table('EstCurso')->insert(
            array(
             'estado' => 'En Curso', 
            ) 
        ); 
        DB::table('EstCurso')->insert(
            array(
             'estado' => 'Terminado', 
            ) 
        ); 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('EstCurso');
    }
}
