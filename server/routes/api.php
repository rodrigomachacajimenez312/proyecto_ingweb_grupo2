<?php

use App\Http\Controllers\QRController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::apiResource('/alumno', 'AlumnoController');
Route::post('/alumno/actualizar/{id}', 'AlumnoController@update');
Route::get('/alumno/delete/{id}', 'AlumnoController@destroy');

Route::apiResource('/certificado', 'CertificadosCumplidosCursoController');
Route::post('/certificado/actualizar/{id}', 'CertificadosCumplidosCursoController@update');
Route::get('/certificado/delete/{id}', 'CertificadosCumplidosCursoController@destroy');

Route::apiResource('/curso', 'CursoController');
Route::post('/curso/actualizar/{id}', 'CursoController@update');
Route::post('/curso/create', 'CursoController@store');
Route::get('/curso/get/{id}', 'CursoController@show');
Route::get('/curso/get2/{id}', 'CursoController@show2');

Route::group(['prefix' => 'alumno'], function () {
    Route::resource('/', QRController::class);
    Route::get('qr/{id}', [QRController::class,'generar_qr']);
    Route::get('qr_mail/{id}', [QRController::class,'mail_qr']);

});
